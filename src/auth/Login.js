import { ActivityIndicator, ImageBackground, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import { Alert, Box, Button, Center, CloseIcon, Container, Divider, HStack, IconButton, ScrollView, Text, useToast, View, VStack } from 'native-base'
import I from 'react-native-vector-icons/Ionicons'
import TextBox from '../Common/TextBox'
import { useNavigation } from '@react-navigation/native'
import { Base_url } from '../Common/Const'
import { useDispatch } from 'react-redux'
import { userAction } from '../redux/userSlice'

const Login = () => {

    const navigation = useNavigation();
const [loader, setloader] = useState(false)
const [email, setemail] = useState('');
const [password, setpassword] = useState('')
const toast = useToast()
const dispatch = useDispatch();
const errorToast =(type)=>{
    return (    
    
    <Alert w="100%" status="warning">
         <VStack space={2} flexShrink={1} w="100%">
           <HStack flexShrink={1} space={2} justifyContent="space-between">
             <HStack space={2} flexShrink={1}>
               <Alert.Icon mt="1" />
               <Text fontSize="md" color="coolGray.800">
                {type} 
               </Text>
             </HStack>
             <IconButton onPress={()=> toast.closeAll()} variant="unstyled" _focus={{
           borderWidth: 0
         }} icon={<CloseIcon size="3"  />} _icon={{
           color: "coolGray.600"
         }} />
           </HStack>
         </VStack>
       </Alert>
 
 )
 }
const onEnter =()=>{
    setloader(true)
    if(email.length==0){
        setloader(false)
        toast.show({
          render:()=>errorToast('Email is Required'),
          placement:"top"
            
        })
    }else if( password.length ==0){
        setloader(false)
         
        toast.show({
            render:()=>errorToast("Password is Required"),
            placement:"top"
              
          })
    }else{
      
        let postData = {
        email:email.trim(),
        password:password,

    }
    fetch(Base_url+'auth/login', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body:JSON.stringify(postData)

      
      }).then(res=>res.json())
      .then(res=>{
      setloader(false)
      if(res.result.length==0){
        toast.show({
            render:()=>errorToast(res.message),
            placement:"top"
              
          })
      }else{
          dispatch(userAction.userData(res.result))
          navigation.navigate('Home')
      }
    
        console.log(res)
      }).catch(res=>{
        setloader(false)
        console.log(res)
      });
    }
}


  return (
    <ScrollView >
     <ImageBackground source={require('../asset/sp.png')} style={styles.mapi} >
     <I onPress={()=>navigation.goBack()} name="arrow-back-outline"style={styles.arrow} color={styles.tColor.color} size={35} />
     </ImageBackground>
     <View >
<Text fontSize={'2xl'} paddingX={5} color={styles.tColor.color}>Sign in </Text>

<View marginBottom={5}>
<TextBox Icon={I} value={setemail} lable={'Email'} IconName={'at-outline'}/>
<TextBox Icon={I} value={setpassword} lable={'Password'} type={'password'} IconName={'md-lock-closed'}/>

</View>

<Button backgroundColor={'black'} onPress={onEnter} alignItems='center' margin={5}  isLoading={loader} >
 Continue
</Button>
<Divider marginTop={5}/>
<Center marginTop={5} flexDirection='row'>
 New to Logistics ? <Text fontWeight={'extrabold'} onPress={()=>navigation.navigate('signup')} > Register </Text>
</Center>
     </View>
    </ScrollView>
  )
}

export default Login

const styles = StyleSheet.create({
    tColor:{
        color:"#000"
    },mapi:{
        height:300
    },
    arrow:{
        margin:20
    }
})