import { ActivityIndicator, ImageBackground, StyleSheet } from 'react-native'
import React, { useState } from 'react';
import { Alert, Box, Button, Center, CloseIcon, Container, Divider, HStack, IconButton, ScrollView, Text, useToast, View, VStack } from 'native-base'
import I from 'react-native-vector-icons/Ionicons'
import TextBox from '../Common/TextBox'
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { userAction } from '../redux/userSlice';
import { Base_url } from '../Common/Const';



const Register = () => {
    
    const errorToast =(type)=>{
        return (    
        
        <Alert w="100%" status="warning">
             <VStack space={2} flexShrink={1} w="100%">
               <HStack flexShrink={1} space={2} justifyContent="space-between">
                 <HStack space={2} flexShrink={1}>
                   <Alert.Icon mt="1" />
                   <Text fontSize="md" color="coolGray.800">
                    {type} 
                   </Text>
                 </HStack>
                 <IconButton onPress={()=> toast.closeAll()} variant="unstyled" _focus={{
               borderWidth: 0
             }} icon={<CloseIcon size="3"  />} _icon={{
               color: "coolGray.600"
             }} />
               </HStack>
             </VStack>
           </Alert>
     
     )
     }
const [name, setname] = useState('')
const [email, setemail] = useState('')
const [phone, setphone] = useState('')

const [password, setpassword] = useState('')
const [loader, setloader] = useState(false)

const dispatch = useDispatch();
const toast = useToast()
const sender =()=>{



    setloader(true)
    if(email.length==0){
        setloader(false)
        toast.show({
          render:()=>errorToast('Email is Required'),
          placement:"top"
            
        })
    }else if( password.length ==0){
        setloader(false)
         
        toast.show({
            render:()=>errorToast("Password is Required"),
            placement:"top-right"
              
          })
    }else if(name.length==0){
        setloader(false)
        toast.show({
            render:()=>errorToast('User Name is Required'),
            placement:"top-right"
              
          })
    }else if(phone.length ==0){
        setloader(false)
        toast.show({
            render:()=>errorToast('Phone is Required'),
            placement:"top-right"
              
          })
    }else{
        console.log('yse')
        let postData = {
        first_name:name.trim(),
        email:email.trim(),
        password:password,
        phone_number:phone
    }
    fetch(Base_url+'auth/register', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body:JSON.stringify(postData)

      
      }).then(res=>res.json())
      .then(res=>{
      setloader(false)
      if(res.result.length==0){
        toast.show({
            render:()=>errorToast(res.message),
            placement:"top-right"
              
          })
      }else{
          dispatch(userAction.userData(res.result))
          navigation.navigate('Home')
      }
    
        console.log(res)
      }).catch(res=>{
        setloader(false)
      });
    }

    
}
    
    const navigation = useNavigation();
  return (
    <ScrollView >
    <ImageBackground source={require('../asset/sp1.png')} style={styles.mapi} >
    <I onPress={()=>navigation.goBack()} name="arrow-back-outline"style={styles.arrow} color={styles.tColor.color} size={35} />
    </ImageBackground>
    <View >
<Text fontSize={'2xl'} paddingX={5} color={styles.tColor.color}>Sign Up </Text>

<View >
<TextBox Icon={I} value={setname} lable={'Name'} IconName={'person-circle'}/>
<TextBox Icon={I} value={setphone} lable={'Phone Number'} IconName={'phone-portrait-outline'}/>
<TextBox Icon={I} value={setemail} lable={'Email'} IconName={'at-outline'}/>
<TextBox Icon={I} value={setpassword} type={'password'} lable={'Password'} IconName={'md-lock-closed'}/>

</View>
{/* <Container alignSelf={'center'}                         >
   <Text fontSize={'xs'}> By Sigup your're agree to our  Terms & Conditions and Privacy Policy</Text> 
</Container> */}
<Button backgroundColor={'black'} alignItems='center' onPress={sender} margin={5} isLoading={loader} >
Continue
</Button>
<Divider/>
<Center marginTop={5} flexDirection='row' >
<Text color='coolGray.800' fontWeight={'extrabold'}>Join us Before ? </Text><Text  onPress={()=>navigation.navigate('signin')} > Login </Text>
</Center>
    </View>
   </ScrollView>
  )
}

export default Register

const styles = StyleSheet.create({ tColor:{
    color:"#000"
},mapi:{
    height:160
},
arrow:{
    margin:20
}})