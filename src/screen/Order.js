import { View, Text } from 'native-base'
import React, { useEffect, useRef } from 'react'
import I from "react-native-vector-icons/Ionicons"
import { Animated } from 'react-native'
import { transform } from '@babel/core';
import { useNavigation } from '@react-navigation/native';

export default function Order() {
    const navigation = useNavigation()
    const translation = useRef(new Animated.Value(0)).current;
    console.log(translation)
    useEffect(() => {
        Animated.timing(translation, {
          toValue: 10,
          duration:10000,
          useNativeDriver:true
        }).start();
      }, [translation]);

useEffect(() => {
 
    setTimeout(() => {
        navigation.navigate('Home')
    }, 3000);
}, [])



const dt ={
    alignSelf:'center',
    backgroundColor:'#000',
    padding:8,
    borderRadius:150,
   
}

  return (
    <View flex={1} justifyContent='center'>
        <Animated.View style={[dt,{opacity:translation}]}>
            <I name='checkmark' color={'#fff'} size={100}/>
        </Animated.View>
        
      <Text alignSelf={'center'} fontSize={25}>Order successfully</Text>
    </View>
  )
}