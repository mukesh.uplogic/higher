
import { useNavigation } from '@react-navigation/native'
import { Box, Button, Divider, FlatList, ScrollView, Text, View } from 'native-base'
import React from 'react'
import { Image, TouchableOpacity } from 'react-native'
import I from 'react-native-vector-icons/MaterialCommunityIcons'
import { useDispatch, useSelector } from 'react-redux'
import { cartActions } from '../redux/Cartslice'
export default function Cart() {

const data = useSelector(state=>state.cart.product)
const dis = useDispatch()
const navigation = useNavigation()
let total=0;
if (data.length !=[]) {
   total = data.map(item => item.price * item.qty).reduce((prev, next) => prev + next)
} 

console.log('ewew',data)
const clearCart =()=>{
  navigation.navigate('order')
  dis(cartActions.cartToAdd())
  dis(cartActions.pushOrder([...data]))
  
}
const render =({item})=>{
  return(
    <Box  height={150} flexDirection={'row'} justifyContent={'space-between'} background={'#fff'} marginY={1}  rounded="lg">
    <Box alignSelf={'center'} ><Image style={{height:100,width:100}} source={{uri:item.image}}/></Box>
    <Box justifyContent={'space-evenly'}  width={180}>
    <Text isTruncated >{item.name}</Text>
    <Text isTruncated>₹{item.price}</Text>
    <View  flexDirection={'row'}>
      <Button backgroundColor={'gray.500'} onPress={()=> dis(cartActions.qtyAdd(item.product_id))} height={10}><Text color={'#fff'}>+</Text></Button>
      <Text marginX={2}>{item.qty}</Text>
      <Button backgroundColor={'gray.500'} onPress={()=> dis(cartActions.decQty(item.product_id))} disabled={item.qty == 1? true:false} height={10}><Text color={'#fff'}>-</Text></Button>

    </View>
    
    </Box>
    <Box margin={2}  alignSelf={'flex-end'}><Button onPress={()=> dis(cartActions.removeFromCart(item.product_id))} backgroundColor={'#000'}><I name='delete-outline' color={'#fff'} size={20}/></Button></Box>
    </Box>
  )
}


return(
  <View flex={1} justifyContent='center'>
    { data.length > 0 ? 
    <View >
    <ScrollView padding={5}>
            <Text fontWeight={'extrabold'} fontSize={20}>Cart </Text>
   
   
    <FlatList
    
    data={data}
    renderItem={render}
    />

<Text fontSize={20}>Order Info</Text>
      <Box   p="12" backgroundColor={'#fff'} rounded="lg">
        <View flexDirection={'row'} justifyContent={'space-between'}>
         <Box>
          <Text>SubTotal</Text>
          <Text>Tax (10%)</Text>

          

         </Box>
         <Box>
          <Text>₹{total}</Text>
          <Text>₹{(total * 10 /100).toFixed()}</Text>
       
          


         </Box>
          

        </View>
           <Divider/>
           <View flexDirection={'row'} justifyContent={'space-between'}>
             <Text fontSize={23}>Total</Text>
           <Text fontSize={23}>₹{(total +total * 10 /100).toFixed() }</Text>
           </View>
          
      </Box>
   
       </ScrollView>
    <TouchableOpacity>
   <Button margin={1} onPress={clearCart} backgroundColor={'#000'}>Order Now</Button>   
    </TouchableOpacity>
  </View> : <View >
      <Image resizeMode='contain'  style={{width:'100%',height:400}} source={require('../asset/em.png')} ></Image>
      <Text fontSize={20} alignSelf={'center'}>Cart is Empty</Text>
    </View>
  
  }
  </View>
)
}

