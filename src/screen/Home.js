import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AntDesign from "react-native-vector-icons/Ionicons"
import { background } from 'native-base/lib/typescript/theme/styled-system';
import { View } from 'native-base';
import HomeScreen from '../Home/HomeScreen';
import HomeHeader from '../Home/HomeHeader';
import Viewfav from './Viewfav';
import OrderView from './OrderView';
import SideBar from './Drawer';
import Empty from './Empty';

const Tab = createBottomTabNavigator();


const Handlefocus =(focus,ic)=>{
 return focus ?
  <View backgroundColor={'#fff'} borderRadius={30} padding={2}>
  <AntDesign name={ic} color={"#000"} size={25} />
</View>:<View  padding={2}>
  <AntDesign name={ic} color={"#fff"} size={25} />
</View>

}


function Home() {
  return (
    <Tab.Navigator 
  screenOptions={{
    tabBarShowLabel:false,
    tabBarStyle: { backgroundColor:'#000',margin:5,borderRadius:50,height:60 },
  }}
>
      <Tab.Screen name="homeScreen"  options={{
        
       tabBarIconStyle:{backgroundColor:'red'},
          tabBarIcon: ({ color, size ,focused,}) => (
            Handlefocus(focused,'home')  
          ),header:()=> <HomeHeader/>,
          
        }} component={HomeScreen} />
      <Tab.Screen name="Settings" options={{
        
        tabBarIconStyle:{backgroundColor:'red'},
           tabBarIcon: ({ color, size ,focused}) => (
             Handlefocus(focused,'grid-outline')  
           ),headerShown:false
           
         }}component={Empty} />

            <Tab.Screen name="Cart" options={{
        
        tabBarIconStyle:{backgroundColor:'red'},
           tabBarIcon: ({ color, size ,focused}) => (
             Handlefocus(focused,'md-heart-outline')  
           ),headerShown:false
           
         }}component={Viewfav} />
         <Tab.Screen name="orderView" options={{
        
        tabBarIconStyle:{backgroundColor:'red'},
           tabBarIcon: ({ color, size ,focused}) => (
             Handlefocus(focused,'basket')  
           ),headerShown:false
           
         }}component={OrderView} />
    </Tab.Navigator>
  );
}
 export default Home;