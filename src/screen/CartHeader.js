import { View, Text } from 'native-base'
import I from 'react-native-vector-icons/MaterialIcons'
import React from 'react'
import { useNavigation } from '@react-navigation/native'

const CartHeader = () => {
    const navigation  = useNavigation()
  return (
    <View margin={3} backgroundColor={'#fff'} flexDirection={'row'}>
            <I onPress={()=>navigation.goBack()}  style={{padding:10, borderRadius:50, backgroundColor:'#000',textAlign:'center'}} name="arrow-back-ios" color={'#fff'}  size={20} /> 
      <Text left={100} >Order details</Text>
    </View>
  )
}

export default CartHeader