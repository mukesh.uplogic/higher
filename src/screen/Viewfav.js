
import { ScrollView, Text, View } from 'native-base'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { Base_Domain, Base_url } from '../Common/Const'
import Loader from '../Common/Loader'
import MasonryList from '@react-native-seoul/masonry-list';
import { Dimensions, Image, TouchableOpacity } from 'react-native'
import { FlatGrid } from 'react-native-super-grid'
import { useFocusEffect, useNavigation ,useIsFocused} from '@react-navigation/native'

export default function Viewfav() {

const [fav, setfav] = useState('')
const userDetails = useSelector(state=> state.user.userDetails)
const navigation = useNavigation()
useEffect(() => {
  
  if (navigation.isFocused()) {
    let postData = {
      customer_id:userDetails.id,
      page_number:0
        }
  fetch(Base_url+'favView', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body:JSON.stringify(postData)

    
    }).then(res=>res.json()).then(res=>{
      console.log(res)
      if(res.message_code !== 'no_favorites'){
        setfav(res.result)
        }else{
          setfav('')
        }
    })
    console.log('on',fav)
  }
}, [useIsFocused()]);

const Onres=()=>{
    let postData = {
        customer_id:userDetails.id,
        page_number:0
          }
    fetch(Base_url+'favView', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body:JSON.stringify(postData)

      
      }).then(res=>res.json()).then(res=>{
        console.log(res)
        
        
      })
}






  return (
    <ScrollView background={'#fff'}>
        { fav == '' ?
        <View  height={Dimensions.get('screen').height} justifyContent={'center'} >
        
       
    <Image style={{alignSelf:'center',height:400,width:300}} source={require('../asset/no.png')}></Image>
<Text fontSize={25} marginBottom={5} alignSelf={'center'}>Favourite is Empty</Text>
        </View>
    :
    <FlatGrid

   style={{backgroundColor:'#fff'}}
   data={fav}

   renderItem={({item})=>{
  return( 
    
<TouchableOpacity style={{borderRadius:50,margin:5,elevation:2,borderWidth:1,padding:5,}} >
     <Image borderRadius={50} resizeMode='cover'  style={{width:150,height:200,alignSelf:'center'}} source={{uri:Base_Domain+ item.product_image[0].url}} ></Image>
  
</TouchableOpacity>
       
   
   ) }}
   
   
   
   />

    }
   
    </ScrollView>
  )
}

