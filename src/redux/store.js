import AsyncStorage from "@react-native-async-storage/async-storage";
import { combineReducers, legacy_createStore } from "redux";
import persistReducer from "redux-persist/es/persistReducer";
import persistStore from "redux-persist/es/persistStore";
import Cart from "./Cartslice";
import userSlice from "./userSlice";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
};
const reducers =combineReducers({
    user:userSlice.reducer,
    cart:Cart.reducer
})

const persistedReducer = persistReducer(persistConfig, reducers);

const store = legacy_createStore(persistedReducer);

export const persistor = persistStore(store);

export default store;
