import { createSlice } from "@reduxjs/toolkit";

const Cart = createSlice({
  name: "cart",
  initialState: {
    product: [],
    totalQuantity: 0,
    total: 0,
    loder: 0,
    myOrder:[]
  },
  reducers: {
    addtoCart(state, action) {
      const Item = action.payload;
      // console.log(Item);
      const expItem = state.product.find((item) => item.product_id == Item.product_id);

      if (expItem !== undefined) {
        expItem.qty++;
        // state.total += action.payload.price * action.payload.qty;
      } else {
        state.product.push(Item);
      }
      // console.log(state.product);
    },
    cartToAdd(state, action) {
      state.product =[];
    },
    qtyAdd(state, action) {
      const id = action.payload;
      const expId = state.product.find((item) => item.product_id === id);
      expId.qty++;
    },
    decQty(state, action) {
      const id = action.payload;
      const expId = state.product.find((item) => item.product_id === id);

      expId.qty--;
    },
    removeFromCart(state, action) {
      const id = action.payload;

      const expId = state.product.find((item) => item.product_id === id);

      if (expId) {
        state.totalQuantity--;
        state.product.push(
          (state.product = state.product.filter((item) => item.product_id !== id))
        );
      }
    },
    setload(state, action) {
      state.loder++;
    },
    pushOrder(state,action){
console.log(action.payload)
// state.myOrder.push(action.payload)

    },
    clearOrder(state){
state.myOrder= []
    }
  },
});

export const cartActions = Cart.actions;
export default Cart;

