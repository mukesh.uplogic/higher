import { createSlice } from "@reduxjs/toolkit";




const userSlice = createSlice({
  name: "user",
  initialState: {
   userDetails:[],
   fav:[]
  },
  reducers: {
    userData(state,actions){
console.log(actions.payload)
state.userDetails = actions.payload
    },
    loadFav(state,action){
state.fav = action.payload
    }
  },
});

export const userAction = userSlice.actions;
export default userSlice;

