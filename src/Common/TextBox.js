import { StyleSheet } from 'react-native'
import React from 'react'
import { Box, FormControl, Input, Text, TextField, View, WarningOutlineIcon } from 'native-base'
import I from 'react-native-vector-icons/Ionicons'
const TextBox = ({Icon ,IconName,lable,value,type='text'}) => {
  return (
    // <View >
        <Box alignItems="center" margin={2}>
      <FormControl  w="75%" maxW="300px">
        <FormControl.Label>{lable}</FormControl.Label>
        <Input placeholder={lable} onChangeText={(t)=>value(t)} type={type} variant='filled' InputLeftElement={<Icon name={IconName}   size={25}/>}   />
        {/* <TextField backgroundColor={'amber.100'}   InputLeftElement={<Icon name={IconName}   size={25}/>}></TextField> */}
        <FormControl.ErrorMessage backgroundColor={'amber.100'}  leftIcon={<WarningOutlineIcon size="xs" />}>
          Try different from previous passwords.
        </FormControl.ErrorMessage>
      </FormControl>
    </Box>
      
    // </View>
  )
}

export default TextBox

const styles = StyleSheet.create({})