import { ActivityIndicator, Modal, StyleSheet, Text, View } from 'react-native'
import React from 'react'

const Loader = () => {
  return (
    <Modal style={{flex:1,backgroundColor:"red"}} transparent >
      <ActivityIndicator style={{ flex:1,
        position:"relative",
        // backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'center',
        alignSelf: 'center',}} color={'#000'} size={'large'}></ActivityIndicator>
    </Modal>
  )
}

export default Loader

const styles = StyleSheet.create({})