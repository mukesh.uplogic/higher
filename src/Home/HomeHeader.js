import { Dimensions, Pressable, StyleSheet, ToastAndroid } from 'react-native'
import React from 'react'
import { Badge, Stack, Text, View, VStack } from 'native-base'
import I from "react-native-vector-icons/Ionicons"
import { useSelector } from 'react-redux'
import { useNavigation } from '@react-navigation/native'


let ScreenH = Dimensions.get('window').height
// console.log(ScreenH)

const HomeHeader = () => {
const navigate = useNavigation()
const userDetails = useSelector(state=> state.user.userDetails)
const data = useSelector(state=>state.cart.product)
let Count=0;
if(data.length >0){
   Count = data.map(item => item.qty).reduce((prev, next) => prev + next)

}

  return (
    <View flexDirection={'row'} paddingX={3} alignItems={'center'} justifyContent={'space-between'}  height={ScreenH *10 /100} >
        <View padding={2}>
        <Text fontSize={14} color={'gray.500'}>Welcome Back </Text>

             <Text fontWeight={'bold'} fontSize={18} >Hi { userDetails.first_name} ! </Text>    
        </View>
        <Stack space={5} justifyContent={'space-between'} paddingX={5} flexDirection={'row'} >
          <VStack>
            <I name='ios-search-outline' color={"#000"} style={{margin:10}} size={30} />  
          </VStack>
      
        <VStack alignItems={'center'} >

{Count ==0?

<I name='cart-outline' onPress={()=> ToastAndroid.show('Your Cart is Empty',ToastAndroid.SHORT)} style={{marginTop:8}} color={"#000"} size={30} />:
<Pressable onPress={()=> navigate.navigate('cart')}>
<Badge // bg="red.400"

                  colorScheme='gray'
                  rounded="full"
                  mb={-4}
                  mr={-4}
                  zIndex={1}
                  variant="solid"
                  alignSelf="flex-end"
                  _text={{
                    fontSize: 12,
                  }}
                >
                {Count}
                </Badge> 
                <I name='cart-outline' color={"#000"} size={30} />
                </Pressable>
}

                
              </VStack>
       
        


        </Stack>
     
    </View>
  )
}

export default HomeHeader

const styles = StyleSheet.create({})