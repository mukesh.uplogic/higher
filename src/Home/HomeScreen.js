import { Dimensions, ImageBackground, StyleSheet,Image, ScrollView,FlatList, ToastAndroid, BackHandler, TouchableOpacity } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import { Button,  HStack,  Stack,  Text, useToast, View } from 'native-base'
import Carousel from 'react-native-banner-carousel';
import { Base_Domain, Base_url } from '../Common/Const';
import { VStack, Box, Divider } from 'native-base';
import I from "react-native-vector-icons/Ionicons"
import { FlatGrid } from 'react-native-super-grid';
import Loader from '../Common/Loader';
import { useDispatch, useSelector } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
import { cartActions } from '../redux/Cartslice';
import SideBar from  '../screen/Drawer'
const width = Dimensions.get('window').width;


export default function HomeScreen() {
    const [loader, setloader] = useState(false)
const toast = useToast()
const userDetails = useSelector(state=> state.user.userDetails)
const cart = useSelector(state=> state.cart.product)

const dispa = useDispatch()
console.log('user',cart)
    const [indeX, setIndex] = useState(0);
    const [banner, setbanner] = useState([]);
    const [catogory, setCati] = useState([]);
    const [exit, setexit] = useState(0)
    const scrolRef = useRef()
const [filter, setfilter] = useState({
    customer_id:userDetails.id,
    page_number:0,
    category_id:[1]
})
const [FilterData, setFilterData] = useState([])
    

useEffect(() => {
   
    LoadHome()



  
}, [])
const LoadHome =()=>{
    fetch(Base_url+'homeSection', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
      
      }).then(res=>res.json())
      .then(res=>{
        setbanner(res.result[0].data)
        setCati(res.result[6].data)
        // console.log(res.result)
      });
}

const Faver =(type,p_id)=>{
setloader(true)
const urk = type==0? 'addFav':'removeFav';
console.log('ss',p_id)
const posD = {
    customer_id:userDetails.id,
    product_id:p_id
}
fetch(Base_url+urk,{
    method:"POST",
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body:JSON.stringify(posD)
}).then(re=>re.json()).then(res=>{
    console.log(res)
    setloader(false)
    setfilter({
        customer_id:userDetails.id,
    page_number:filter.page_number,
    category_id:filter.category_id
    })
    const text =res.message_code == 'add_to_favorite_successfully'? 'Added to favorite':' Removed from favorite'
    toast.show({
        title:text,
        placement:'top',
        duration:800
    })
   

}).catch(res=>{
    setloader(false)
    console.log(res)
})
}

useEffect(() => {
    reLoadFilter()
}, [filter])
const reLoadFilter =()=>{
    fetch(Base_url+'productFilter',{
        method:"POST",
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body:JSON.stringify(filter)
    }).then(re=>re.json()).then(res=>{
        // console.log(res.result)
    setFilterData(res.result)
    })

}

const backAction = () => {
    setTimeout(() => {
      setexit(0);
    }, 2000); // 2 seconds to tap second-time

    if (exit === 0) {
      setexit(exit + 1);

      ToastAndroid.show("Press back again to exit", ToastAndroid.SHORT);
    } else if (exit === 1) {
      BackHandler.exitApp();
    }
    return true;
  };
  useFocusEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backAction);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backAction);
  });

   const changeProduct =(index,pr)=>{
    console.log(pr)
    setfilter({
        customer_id:userDetails.id,
    page_number:0,
    category_id:[pr.id]
    })
setIndex(index)
   };



   const addtoCart= (value)=>{
    dispa(cartActions.addtoCart({
        qty:1,
        product_id:value.product_id,
        price:value.varient[0].offer_price,
        image:Base_Domain +value.product_image[0].url,
        name:value.product_name
    }))
   }

    const RenderCaro = (item) => {

return(
<Image resizeMode="cover" style={{width:width,height:250}} w source={{
          uri: Base_Domain+ item.image
        }} alt={"Alternate Text"}>
    
</Image>)
    }


    return (
        <ScrollView>
            <View>
                 {/* <Carousel
                    autoplay
                    autoplayTimeout={10000}
                    loop
                    index={0}
                    pageSize={width}
                > 
                 {banner.map((image, index) => RenderCaro(image))}
                 </Carousel> */}
            </View>

<View marginY={5}>
{ loader == true ? <Loader/>:null}

<FlatList
data={catogory}
keyExtractor={(item)=>item.id}
horizontal={true}
renderItem={({item,index})=>{
    return (
        <Button onPress={()=>changeProduct(index,item)} backgroundColor={indeX== index? 'black':'#fff'}  marginX={2} padding={2} w={100} > 
        <Text color={indeX== index? '#fff':'#000'}>
             {item.category_name}
        </Text>
       </Button>
    )
}}

/>

</View>
<View >

    { FilterData.length !==0 ?
    <ScrollView  >
         <FlatGrid
     itemDimension={150}
     spacing={6}
     style={{backgroundColor:"#fff",zIndex:4}}
    data={FilterData}
    renderItem={({item})=>{
        return (< >

           <View>
            <TouchableOpacity>
                       <I onPress={()=>Faver(item.fav_status,item.product_id)} name={item.fav_status ==0 ?'heart-outline':'heart'}style={{position:'absolute',right:0,margin:2,zIndex:2}} color={"#000"} size={20}/>
    
            </TouchableOpacity>
           <Image resizeMode='contain' style={{width:150,height:150}} source={{uri: Base_Domain +item.product_image[0].url}}>
           </Image>
        </View>
        <Text padding={2} isTruncated>{item.product_name}</Text>
          <View justifyContent={'space-between'} flexDirection={'row'} padding={2}>
            <View>
            <Text color={'gray.400'} isTruncated textDecorationLine='line-through' >₹{item.varient[0].actual_price}</Text> 
        <Text isTruncated fontSize={'sm'}>₹{item.varient[0].offer_price}</Text>    
                
            </View>
            <TouchableOpacity alignSelf={'center'}>
      <Button onPress={()=>addtoCart(item)} backgroundColor={'black'} height={10} ><I name='cart-outline'  color={"#fff"} size={20}/></Button>
    
            </TouchableOpacity>
          </View>
        
        
          
        </>)
    }}
    />
 
    </ScrollView>
: <Loader/> 

}
   
</View>

        </ScrollView>
    )
}

const styles = StyleSheet.create({})