// In App.js in a new project

import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as eva from '@eva-design/eva';
import { ApplicationProvider } from '@ui-kitten/components';
import { NativeBaseProvider } from "native-base";
import Home from './src/screen/Home';
import Login from './src/auth/Login';
import Register from './src/auth/Register';
import HomeScreen from './src/Home/HomeScreen';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import store,{persistor} from './src/redux/store';
import Start from './src/Common/Start';
import Cart from './src/screen/Cart';
import CartHeader from './src/screen/CartHeader';
import Order from './src/screen/Order';
import OrderView from './src/screen/OrderView';
const Stack = createNativeStackNavigator();

function App() {
  return (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
           <NativeBaseProvider>
  
       <ApplicationProvider {...eva} theme={eva.light}>
        <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="start" options={{headerShown:false}}  component={Start} />
      <Stack.Screen name="signin" options={{headerShown:false}}  component={Login} />
      <Stack.Screen name="signup" options={{headerShown:false}}  component={Register} />
        <Stack.Screen name="Home" options={{headerShown:false}} component={Home} />
        <Stack.Screen  name="cart" options={{header:()=><CartHeader/>}} component={Cart} />
        <Stack.Screen  name="order" options={{headerShown:false}} component={Order} />
       



      </Stack.Navigator>
    </NavigationContainer>
    </ApplicationProvider>
    </NativeBaseProvider>

    </PersistGate>
  </Provider>

   
 
   
  
  );
}

export default App;